package ru.tsc.pavlov.tm.api.repository;

import ru.tsc.pavlov.tm.api.IRepository;
import ru.tsc.pavlov.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository <E extends AbstractOwnerEntity> extends IRepository<E>  {

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    E add(String userId, final E entity);

    void remove(String userId, final E entity);

    void clear(final String userId);

    boolean existsById(final String id);

    E findById(String userId, String id);

    E removeById(String userId, String id);

    Integer getSize(String userId);

    E findByIndex(String userId, int index);

    E findByName(String userId, String name);

}
