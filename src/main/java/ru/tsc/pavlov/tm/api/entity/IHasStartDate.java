package ru.tsc.pavlov.tm.api.entity;

import java.util.Date;

public interface IHasStartDate {

    Date getStartDate();

    void setStartDate(Date startDate);

}
