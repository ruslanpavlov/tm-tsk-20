package ru.tsc.pavlov.tm.api.repository;

import ru.tsc.pavlov.tm.api.IRepository;
import ru.tsc.pavlov.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

}
