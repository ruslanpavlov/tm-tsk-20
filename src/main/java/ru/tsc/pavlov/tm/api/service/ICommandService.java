package ru.tsc.pavlov.tm.api.service;

import ru.tsc.pavlov.tm.command.AbstractCommand;

import java.util.Collection;


public interface ICommandService {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    Collection<String> getListCommandName();

    Collection<String> getListCommandArgs();

    void add(AbstractCommand command);

}
