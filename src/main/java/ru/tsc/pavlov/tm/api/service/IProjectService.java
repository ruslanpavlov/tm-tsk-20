package ru.tsc.pavlov.tm.api.service;

import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.model.Project;

public interface IProjectService extends IOwnerService<Project> {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    Project findByName(String userId, String name);

    Project findByIndex(String userId, Integer index);

    Project updateById(String userId, final String id, final String name, final String index);

    Project updateByIndex(String userId, final Integer index, final String name, final String description);

    boolean existsByIndex(String userId, int index);

    Project startById(String userId, String id);

    Project startByIndex(String userId, Integer index);

    Project startByName(String userId, String name);

    Project finishById(String userId, String id);

    Project finishByIndex(String userId, Integer index);

    Project finishByName(String userId, String name);

    Project changeStatusById(String userId, String id, Status status);

    Project changeStatusByIndex(String userId, Integer index, Status status);

    Project changeStatusByName(String userId, String name, Status status);

}
