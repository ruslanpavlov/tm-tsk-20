package ru.tsc.pavlov.tm.api.service;

import ru.tsc.pavlov.tm.model.User;

public interface IAuthService {

    String getCurrentUserId();

    void setCurrentUserId(String userId);

    void setUser(String currentUserId);

    boolean isUserAuth();

    boolean isUserAdmin();

    User login(String login, String password);

    void logout();

}
