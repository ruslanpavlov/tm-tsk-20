package ru.tsc.pavlov.tm.api;

import ru.tsc.pavlov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity>{

    void add(final E entity);

    void remove(final E entity);

    List<E> findAll();

    List<E> findAll(Comparator<E> comparator);

    void clear();

    boolean existsById(final String id);

    E findById(String id);

    E removeById(String userId, String id);

    Integer getSize();

}
