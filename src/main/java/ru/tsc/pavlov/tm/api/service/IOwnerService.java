package ru.tsc.pavlov.tm.api.service;

import ru.tsc.pavlov.tm.api.IService;
import ru.tsc.pavlov.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService <E extends AbstractOwnerEntity> extends IService<E> {

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    E findById(String userId, String id);

    E findByIndex(String userId, Integer index);

    void clear(String userId);

    void remove(String userId, E entity);

    Integer getSize(String userId);

}
