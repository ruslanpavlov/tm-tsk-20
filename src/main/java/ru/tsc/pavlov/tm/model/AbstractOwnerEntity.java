package ru.tsc.pavlov.tm.model;

public class AbstractOwnerEntity extends AbstractEntity{

    private String userId;

    private String name;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

}
