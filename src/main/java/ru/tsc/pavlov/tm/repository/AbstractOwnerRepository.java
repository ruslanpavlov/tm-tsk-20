package ru.tsc.pavlov.tm.repository;

import ru.tsc.pavlov.tm.api.repository.IOwnerRepository;
import ru.tsc.pavlov.tm.exception.AbstractException;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.model.AbstractOwnerEntity;
import ru.tsc.pavlov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerRepository <E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public E add(String userId, E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(final String userId, final E entity) {
        final List<E> list = findAll(userId);
        list.remove(entity);
    }

    @Override
    public List<E> findAll(final String userId) {
        final List<E> entities = new ArrayList<>();
        for (E entity : list) {
            if (entity.getUserId().equals(userId)) entities.add(entity);
        }
        return entities;
    }

    @Override
    public List<E> findAll(final String userId, Comparator<E> comparator) {
        final List<E> list = findAll(userId);
        list.sort(comparator);
        return list;
    }

    @Override
    public E findById(final String userId, final String id) {
        for (E entity : list) {
            if (entity.getId().equals(id)) return entity;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public E removeById(final String userId, final String id) {
        final E entity = findById(userId, id);
        if (entity == null) return null;
        remove(userId, entity);
        return entity;
    }

    @Override
    public Integer getSize(final String userId) {
        final List<E> list = findAll(userId);
        return list.size();
    }

    @Override
    public E findByName(final String userId, final String name) {
        List<E> entities = findAll(userId);
        for (E entity : entities) {
            if (entity.getName().equals(name)) return entity;
        }
        throw new AbstractException("Error. Entity not found");
    }

    @Override
    public E findByIndex(final String userId, final int index) {
        List<E> entities = findAll(userId);
        return entities.get(index);
    }

    @Override
    public void clear(final String userId) {
        final List<E> list = findAll(userId);
        list.clear();
    }

}
