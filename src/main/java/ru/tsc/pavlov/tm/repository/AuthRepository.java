package ru.tsc.pavlov.tm.repository;

import ru.tsc.pavlov.tm.api.repository.IAuthRepository;

public class AuthRepository implements IAuthRepository {

    private String currentUserId;

    @Override
    public String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public void setCurrentUserId(String currentUserId) {
        this.currentUserId = currentUserId;
    }

}
