package ru.tsc.pavlov.tm.service;

import ru.tsc.pavlov.tm.api.repository.ITaskRepository;
import ru.tsc.pavlov.tm.api.service.ITaskService;
import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.exception.empty.EmptyDescriptionException;
import ru.tsc.pavlov.tm.exception.empty.EmptyIdException;
import ru.tsc.pavlov.tm.exception.empty.EmptyIndexException;
import ru.tsc.pavlov.tm.exception.empty.EmptyNameException;
import ru.tsc.pavlov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.pavlov.tm.exception.system.IndexIncorrectException;
import ru.tsc.pavlov.tm.model.Task;


import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String userId, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public Task findByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new TaskNotFoundException();
        return taskRepository.findByName(userId, name);
    }

    @Override
    public Task findById(String userId, String id) {
        return taskRepository.findById(userId, id);
    }

    @Override
    public Task findByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) return null;
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        return taskRepository.findByIndex(userId, index);
    }

    @Override
    public void remove(String userId, Task entity) {

    }

    @Override
    public Integer getSize(String userId) {
        return null;
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeByName(userId, name);
    }

    @Override
    public Task removeByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        return taskRepository.removeByIndex(userId, index);
    }

    @Override
    public Task updateById(
            final String userId, final String id, final String name, final String description
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findById(id);
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(
            final String userId, final Integer index, final String name, final String description
    ) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        final Task task = taskRepository.findByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public boolean existsByIndex(final String userId, final int index) {
        return taskRepository.existsByIndex(userId, index);
    }

    @Override
    public boolean existsByName(final String userId, final String name) {
        return taskRepository.existsByName(userId, name);
    }

    @Override
    public Task startById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.startById(userId, id);
    }

    @Override
    public Task startByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        return taskRepository.startByIndex(userId, index);
    }

    @Override
    public Task startByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.startByName(userId, name);
    }

    @Override
    public Task finishById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.finishById(userId, id);
    }

    @Override
    public Task finishByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        return taskRepository.finishByIndex(userId, index);
    }

    @Override
    public Task finishByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.finishByName(userId, name);
    }

    @Override
    public Task changeStatusById(final String userId, final String id, Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) status = Status.valueOf("IN PROGRESS");
        return taskRepository.changeStatusById(userId, id, status);
    }

    @Override
    public Task changeStatusByIndex(final String userId, final Integer index, Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        if (status == null) status = Status.valueOf("IN PROGRESS");
        return taskRepository.changeStatusByIndex(userId, index, status);
    }

    @Override
    public Task changeStatusByName(final String userId, final String name, Status status) {
        if (name == null || name.isEmpty()) return null;
        if (status == null) status = Status.valueOf("IN PROGRESS");
        return taskRepository.changeStatusByName(userId, name, status);
    }

}
