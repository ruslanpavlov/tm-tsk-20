package ru.tsc.pavlov.tm.service;

import ru.tsc.pavlov.tm.api.repository.IProjectRepository;
import ru.tsc.pavlov.tm.api.repository.ITaskRepository;
import ru.tsc.pavlov.tm.api.service.IProjectTaskService;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.pavlov.tm.model.Project;
import ru.tsc.pavlov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @Override
    public Task bindTaskById(final String userId, final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException(projectId);
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return null;
        return taskRepository.bindTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    public Task unbindTaskById(final String userId, final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException(projectId);
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return null;
        return taskRepository.unbindTaskById(userId, taskId);
    }

    @Override
    public Project removeById(final String userId, final String projectId) {
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

    @Override
    public Project removeByIndex(final String userId, final Integer index) {
        final Project project = projectRepository.findByIndex(userId, index);
        String projectId = project.getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        final Project project = projectRepository.findByName(userId, name);
        String projectId = project.getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

}
