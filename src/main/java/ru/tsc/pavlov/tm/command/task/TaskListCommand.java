package ru.tsc.pavlov.tm.command.task;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.Sort;
import ru.tsc.pavlov.tm.model.Task;
import ru.tsc.pavlov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_LIST;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "List tasks";
    }

    @Override
    public void execute() {
        List<Task> tasks;
        System.out.println("[ENTER SORT VALUES]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        final String userId = getAuthService().getCurrentUserId();
        if (sort != null || !sort.isEmpty()) {
            Sort sortType = Sort.valueOf(sort);
            tasks = getTaskService().findAll(userId, sortType.getComparator());
        } else
            tasks = getTaskService().findAll(userId);

        if (tasks.size() <= 0) {
            System.out.println("Project list is empty now");
            return;
        }

        System.out.println("[LIST PROJECTS]");
        for (Task task : tasks)
            showTask(task);
        System.out.println("[OK]");
    }

}
