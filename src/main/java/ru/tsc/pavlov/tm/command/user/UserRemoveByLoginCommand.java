package ru.tsc.pavlov.tm.command.user;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractUserCommand {
    @Override
    public String getName() {
        return TerminalConst.USER_REMOVE_BY_LOGIN_COMMAND;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove user by login";
    }

    @Override
    public void execute() {
        System.out.println("PLEASE ENTER USER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final User user = getUserService().removeByLogin(login);
        System.out.println("USER DELETED");
    }

}
