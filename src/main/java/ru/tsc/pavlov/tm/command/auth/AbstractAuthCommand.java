package ru.tsc.pavlov.tm.command.auth;

import ru.tsc.pavlov.tm.api.service.IAuthService;
import ru.tsc.pavlov.tm.command.AbstractCommand;

public abstract class AbstractAuthCommand extends AbstractCommand {

    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

}
