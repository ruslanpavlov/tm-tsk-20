package ru.tsc.pavlov.tm.command.mutually;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.exception.entity.MutualNotFoundException;
import ru.tsc.pavlov.tm.model.Task;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class TaskAddToProjectByIdCommand extends AbstractMutuallyCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_ADD_TO_PROJECT_BY_ID;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Add task to project by id";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]");
        final String taskId = TerminalUtil.nextLine();
        final Task taskUpdated = getProjectTaskService().bindTaskById(userId, projectId, taskId);
        if (taskUpdated == null) throw new MutualNotFoundException(taskId, projectId);
        System.out.println("[OK]");
    }

}
