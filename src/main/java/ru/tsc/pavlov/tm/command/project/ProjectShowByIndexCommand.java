package ru.tsc.pavlov.tm.command.project;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.model.Project;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return TerminalConst.PROJECT_SHOW_BY_INDEX;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show project by index";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
