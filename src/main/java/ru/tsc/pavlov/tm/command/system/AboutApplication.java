package ru.tsc.pavlov.tm.command.system;

import ru.tsc.pavlov.tm.command.AbstractCommand;
import ru.tsc.pavlov.tm.constant.TerminalConst;

public class AboutApplication extends AbstractCommand {

    @Override
    public String getName() {
        return TerminalConst.VERSION;
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Display developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Ruslan Pavlov");
        System.out.println("E-MAIL: o000.ruslan@yandex.ru");
    }

}
