package ru.tsc.pavlov.tm.command;

import ru.tsc.pavlov.tm.api.service.ServiceLocator;
import ru.tsc.pavlov.tm.util.StringUtil;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract void execute();

    public final String toString() {
        String result = "";
        String name = getName();
        String arg = getArgument();
        String description = getDescription();

        if (!StringUtil.isEmpty(name)) result += name;
        if (!StringUtil.isEmpty(arg)) result += " " + arg + " ";
        if (!StringUtil.isEmpty(description)) result += " - " + description;
        return result;
    }

}
