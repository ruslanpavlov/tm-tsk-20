package ru.tsc.pavlov.tm.command.mutually;

import ru.tsc.pavlov.tm.api.service.IAuthService;
import ru.tsc.pavlov.tm.api.service.IProjectTaskService;
import ru.tsc.pavlov.tm.command.AbstractCommand;

public abstract class AbstractMutuallyCommand extends AbstractCommand {

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    protected IAuthService getAuthService() { return serviceLocator.getAuthService(); }

}
