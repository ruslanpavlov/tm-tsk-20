package ru.tsc.pavlov.tm.exception.empty;

import ru.tsc.pavlov.tm.exception.AbstractException;

public class EmptyIndexException extends AbstractException {

    public EmptyIndexException() {
        super("Error: Index is empty.");
    }

}
