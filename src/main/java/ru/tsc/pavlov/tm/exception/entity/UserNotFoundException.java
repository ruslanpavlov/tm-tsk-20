package ru.tsc.pavlov.tm.exception.entity;

import ru.tsc.pavlov.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException(){
        super("Error. User not found");
    }

    public UserNotFoundException(String value){
        super("Error. User " + value + " not found");
    }

}
