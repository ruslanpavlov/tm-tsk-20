package ru.tsc.pavlov.tm.exception.entity;

import ru.tsc.pavlov.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error: Project not found.");
    }

}
